import { expect } from 'chai'
import { shallowMount } from '@vue/test-utils'
import CotizrFooter from '@/components/CotizrFooter.vue'

describe('CotizrFooter.vue', () => {
  const wrapper = shallowMount(CotizrFooter)
  it('its empty', () => {
    // eslint-disable-next-line no-unused-expressions
    expect(wrapper.text()).to.be.empty
  })
})
