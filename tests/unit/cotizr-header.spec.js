import { expect } from 'chai'
import { shallowMount } from '@vue/test-utils'
import CotizrHeader from '@/components/CotizrHeader.vue'

describe('CotizrHeader.vue', () => {
  const wrapper = shallowMount(CotizrHeader)
  it('renders COTIZR', () => {
    expect(wrapper.text()).to.include('COTIZR')
  })
})
