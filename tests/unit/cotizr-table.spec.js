import { expect } from 'chai'
import { shallowMount } from '@vue/test-utils'
import CotizrTable from '@/components/CotizrTable.vue'

describe('CotizrTable.vue', () => {
  const wrapper = shallowMount(CotizrTable)
  it('renders table element', () => {
    // eslint-disable-next-line no-unused-expressions
    expect(wrapper.contains('table')).to.be.true
  })
})
