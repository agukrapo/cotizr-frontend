import { expect } from 'chai'
import { shallowMount } from '@vue/test-utils'
import CotizrRow from '@/components/CotizrRow.vue'

describe('CotizrRow.vue', () => {
  const value = {
    actualizado: '2019-08-05T14:14:09.862032-03:00',
    compra: 3.45,
    entidad: 'E-N-T-I-D-A-D',
    venta: 6.78
  }

  const wrapper = shallowMount(CotizrRow, {
    propsData: { value }
  })
  it('renders Entidad', () => {
    expect(wrapper.text()).to.include('E-N-T-I-D-A-D')
  })
})
