import { expect } from 'chai'
import { shallowMount } from '@vue/test-utils'
import CotizrColumnHeader from '@/components/CotizrColumnHeader.vue'

describe('CotizrColumnHeader.vue', () => {
  const wrapper = shallowMount(CotizrColumnHeader, {
    propsData: { field: 'f-i-e-l-d' }
  })
  it('renders TITULO', () => {
    expect(wrapper.text()).to.include('F-i-e-l-d')
  })
})
