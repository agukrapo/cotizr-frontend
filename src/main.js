import Vue from 'vue'
import Cotizr from './Cotizr.vue'
import { library } from '@fortawesome/fontawesome-svg-core'
import { faExternalLinkAlt, faCaretUp, faCaretDown, faSpinner } from '@fortawesome/free-solid-svg-icons'
import { FontAwesomeIcon } from '@fortawesome/vue-fontawesome'

library.add(faExternalLinkAlt, faCaretUp, faCaretDown, faSpinner)
Vue.component('font-awesome-icon', FontAwesomeIcon)

new Vue({
  render: h => h(Cotizr)
}).$mount('#cotizr')
