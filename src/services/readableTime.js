export default {
  between: function (dateA, dateB) {
    const seconds = Math.floor((dateA - dateB) / 1000)
    let intervalType

    let interval = Math.floor(seconds / 31536000)
    if (interval >= 1) {
      intervalType = 'año'
    } else {
      interval = Math.floor(seconds / 2592000)
      if (interval >= 1) {
        intervalType = 'mes'
      } else {
        interval = Math.floor(seconds / 86400)
        if (interval >= 1) {
          intervalType = 'dia'
        } else {
          interval = Math.floor(seconds / 3600)
          if (interval >= 1) {
            intervalType = 'hora'
          } else {
            interval = Math.floor(seconds / 60)
            if (interval >= 1) {
              intervalType = 'minuto'
            } else {
              interval = seconds
              intervalType = 'segundo'
            }
          }
        }
      }
    }

    if (interval > 1 || interval === 0) {
      if (intervalType === 'mes') {
        intervalType += 'es'
      } else {
        intervalType += 's'
      }
    }

    return `Hace ${interval} ${intervalType}`
  }
}
