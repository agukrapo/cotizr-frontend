const correncyFormat = new Intl.NumberFormat('es-AR', { style: 'currency', currency: 'ARS' })

export default {
  format: correncyFormat.format
}
