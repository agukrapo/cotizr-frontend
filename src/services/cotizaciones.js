
export default async () => {
  const rootURL = process.env.VUE_APP_ROOT_URL
  if (!rootURL) {
    throw new Error('invalid root URL')
  }
  let response = await fetch(`${rootURL}/api/cotizaciones`)
  let data = await response.json()
  return Object.values(data)
}
